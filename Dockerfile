FROM node:18-alpine
 
WORKDIR /usr/src/app
 
COPY package.json ./
 
COPY apps/docs/package.json ./apps/docs/package.json
 
RUN npm install
 
COPY . .
 
EXPOSE 8080
 
CMD [ "node", "apps/docs/server.js" ]